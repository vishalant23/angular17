import { Component,ViewEncapsulation } from '@angular/core';
import { LoaderService } from '../../../shared/service/loader.service';
@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrl: './spinner.component.scss',
  encapsulation: ViewEncapsulation.ShadowDom
})
export class SpinnerComponent {

  constructor(public loader: LoaderService) { }

}
