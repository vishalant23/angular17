import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BranchesRoutingModule } from './branches-routing.module';
import { BranchesFormComponent } from './branches-form/branches-form.component';
import { BranchesTableComponent } from './branches-table/branches-table.component';
@NgModule({
  declarations: [BranchesFormComponent,BranchesTableComponent],
  imports: [
    CommonModule,
    BranchesRoutingModule
  ]
})

export class BranchesModule { }
