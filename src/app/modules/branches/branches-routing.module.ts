import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BranchesFormComponent } from './branches-form/branches-form.component';
import { BranchesTableComponent } from './branches-table/branches-table.component';
const routes: Routes = [
  // { path: '', redirectTo: '/branches-table', pathMatch: 'full' },
  { path: 'branches-form', component: BranchesFormComponent },
  { path: 'branches-table', component: BranchesTableComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BranchesRoutingModule { }
