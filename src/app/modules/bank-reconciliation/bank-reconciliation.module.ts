import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BankReconciliationRoutingModule } from './bank-reconciliation-routing.module';
import { BankReconciliationTableComponent } from './bank-reconciliation-table/bank-reconciliation-table.component';

@NgModule({
  declarations: [BankReconciliationTableComponent],
  imports: [
    CommonModule,
    BankReconciliationRoutingModule
  ]
})
export class BankReconciliationModule { }
