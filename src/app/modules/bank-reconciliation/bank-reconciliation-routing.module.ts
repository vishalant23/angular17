import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BankReconciliationTableComponent } from './bank-reconciliation-table/bank-reconciliation-table.component';
const routes: Routes = [
  {path:'bank-reconciliation-table',component:BankReconciliationTableComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BankReconciliationRoutingModule { }
