import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientsFormComponent } from './clients-form/clients-form.component';
import { ClientsTableComponent } from './clients-table/clients-table.component';
const routes: Routes = [
  {path:'clients-form',component:ClientsFormComponent},
  {path:'clients-table',component:ClientsTableComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
