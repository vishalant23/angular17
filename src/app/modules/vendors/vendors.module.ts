import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorsRoutingModule } from './vendors-routing.module';
import { VendorsFormComponent } from './vendors-form/vendors-form.component';
import { VendorsTableComponent } from './vendors-table/vendors-table.component';
@NgModule({
  declarations: [VendorsFormComponent,VendorsTableComponent],
  imports: [
    CommonModule,
    VendorsRoutingModule
  ]
})
export class VendorsModule { }
