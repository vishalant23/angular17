import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VendorsFormComponent } from './vendors-form/vendors-form.component';
import { VendorsTableComponent } from './vendors-table/vendors-table.component';
const routes: Routes = [
  {path:'vendors-table',component:VendorsTableComponent},
  {path:'vendors-form',component:VendorsFormComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorsRoutingModule { }
