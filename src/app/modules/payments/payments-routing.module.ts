import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentsFormComponent } from './payments-form/payments-form.component';
import { PaymentsTableComponent } from './payments-table/payments-table.component';
const routes: Routes = [
  {path:'payments-form',component:PaymentsFormComponent},
  {path:'payments-table',component:PaymentsTableComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule { }
