import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentsRoutingModule } from './payments-routing.module';
import { PaymentsFormComponent } from './payments-form/payments-form.component';
import { PaymentsTableComponent } from './payments-table/payments-table.component';


@NgModule({
  declarations: [
    PaymentsFormComponent,
    PaymentsTableComponent
  ],
  imports: [
    CommonModule,
    PaymentsRoutingModule
  ]
})
export class PaymentsModule { }
