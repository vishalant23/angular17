import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExpenseFormComponent } from './expense-form/expense-form.component';
import { ExpenseTableComponent } from './expense-table/expense-table.component';

const routes: Routes = [
  {path:'expense-form', component:ExpenseFormComponent},
  {path:'expense-table', component:ExpenseTableComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpenseRoutingModule { }
