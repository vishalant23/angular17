import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExpenseRoutingModule } from './expense-routing.module';
import { ExpenseFormComponent } from './expense-form/expense-form.component';
import { ExpenseTableComponent } from './expense-table/expense-table.component';
@NgModule({
  declarations: [ExpenseFormComponent,ExpenseTableComponent],
  imports: [
    CommonModule,
    ExpenseRoutingModule
  ]
})
export class ExpenseModule { }
