import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PurchaseOrderFormComponent } from './purchase-order-form/purchase-order-form.component';
import { PurchaseOrderTableComponent } from './purchase-order-table/purchase-order-table.component';
const routes: Routes = [
  {path:'purchase-order-form',component:PurchaseOrderFormComponent},
  {path:'purchase-order-table',component:PurchaseOrderTableComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseOrderRoutingModule { }
