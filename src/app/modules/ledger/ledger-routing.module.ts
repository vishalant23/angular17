import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LedgerTableComponent } from './ledger-table/ledger-table.component';

const routes: Routes = [
  {path:'ledger-table',component:LedgerTableComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LedgerRoutingModule { }
