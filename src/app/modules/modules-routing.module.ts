import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { MastersComponent } from './masters/masters.component';
const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'access-control', loadChildren: () => import('../modules/access-control/access-control.module').then(m => m.AccessControlModule) },
  { path: 'branches', loadChildren: () => import('../modules/branches/branches.module').then(m => m.BranchesModule) },
  { path: 'employee', loadChildren: () => import('../modules/employee/employee.module').then(m => m.EmployeeModule) },
  {path:'clients',loadChildren:()=>import('../modules/clients/clients.module').then(m=>m.ClientsModule)},
  {path:'vendors',loadChildren:()=>import('../modules/vendors/vendors.module').then(m=>m.VendorsModule)},
  {path:'category',loadChildren:()=>import('../modules/category/category.module').then(m=>m.CategoryModule)},
  {path:'products',loadChildren:()=>import('../modules/products/products.module').then(m=>m.ProductsModule)},
  {path:'expense',loadChildren:()=>import('../modules/expense/expense.module').then(m=>m.ExpenseModule)},
  {path:'enquiry',loadChildren:()=>import('../modules/enquiry/enquiry.module').then(m=>m.EnquiryModule)},
  {path:'quotation',loadChildren:()=>import('../modules/quotation/quotation.module').then(m=>m.QuotationModule)},
  {path:'sales-order',loadChildren:()=>import('../modules/sales-order/sales-order.module').then(m=>m.SalesOrderModule)},
  {path:'packing-slip',loadChildren:()=>import('../modules/packing-slip/packing-slip.module').then(m=>m.PackingSlipModule)},
  {path:'sales-bill',loadChildren:()=>import('../modules/sales-bill/sales-bill.module').then(m=>m.SalesBillModule)},
  {path:'purchase-order',loadChildren:()=>import('../modules/purchase-order/purchase-order.module').then(m=>m.PurchaseOrderModule)},
  {path:'purchase-bill',loadChildren:()=>import('../modules/purchase-bill/purchase-bill.module').then(m=>m.PurchaseBillModule)},
  {path:'proforma-invoice',loadChildren:()=>import('../modules/proforma-invoice/proforma-invoice.module').then(m=>m.ProformaInvoiceModule)},
  {path:'stock',loadChildren:()=>import('../modules/stock/stock.module').then(m=>m.StockModule)},
  {path:'tickets',loadChildren:()=>import('../modules/tickets/tickets.module').then(m=>m.TicketsModule)},
  {path:'payments',loadChildren:()=>import('../modules/payments/payments.module').then(m=>m.PaymentsModule)},
  {path:'log-activity',loadChildren:()=>import('../modules/log-activity/log-activity.module').then(m=>m.LogActivityModule)},
  {path:'work-order',loadChildren:()=>import('../modules/work-order/work-order.module').then(m=>m.WorkOrderModule)},
  {path:'bank-reconciliation',loadChildren:()=>import('../modules/bank-reconciliation/bank-reconciliation.module').then(m=>m.BankReconciliationModule)},
  {path:'debit-note',loadChildren:()=>import('../modules/debit-note/debit-note.module').then(m=>m.DebitNoteModule)},
  {path:'credit-note',loadChildren:()=>import('../modules/credit-note/credit-note.module').then(m=>m.CreditNoteModule)},
  {path:'ledger',loadChildren:()=>import('../modules/ledger/ledger.module').then(m=>m.LedgerModule)},
  {path:'company-profile',loadChildren:()=>import('../modules/company-profile/company-profile.module').then(m=>m.CompanyProfileModule)},
  {path:'change-password',loadChildren:()=>import('../modules/change-password/change-password.module').then(m=>m.ChangePasswordModule)},
  {path:'masters',component: MastersComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
