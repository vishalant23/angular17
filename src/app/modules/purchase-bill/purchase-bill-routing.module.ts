import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PurchaseBillFormComponent } from './purchase-bill-form/purchase-bill-form.component';
import { PurchaseBillTableComponent } from './purchase-bill-table/purchase-bill-table.component';
const routes: Routes = [
  {path:'purchase-bill-form',component:PurchaseBillFormComponent},
  {path:'purchase-bill-table',component:PurchaseBillTableComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseBillRoutingModule { }
