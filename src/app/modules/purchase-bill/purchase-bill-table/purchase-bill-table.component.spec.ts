import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseBillTableComponent } from './purchase-bill-table.component';

describe('PurchaseBillTableComponent', () => {
  let component: PurchaseBillTableComponent;
  let fixture: ComponentFixture<PurchaseBillTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PurchaseBillTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PurchaseBillTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
