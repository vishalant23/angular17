import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PurchaseBillRoutingModule } from './purchase-bill-routing.module';
import { PurchaseBillFormComponent } from './purchase-bill-form/purchase-bill-form.component';
import { PurchaseBillTableComponent } from './purchase-bill-table/purchase-bill-table.component';
@NgModule({
  declarations: [PurchaseBillFormComponent,PurchaseBillTableComponent],
  imports: [
    CommonModule,
    PurchaseBillRoutingModule
  ]
})
export class PurchaseBillModule { }
