import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EnquiryFormComponent } from './enquiry-form/enquiry-form.component';
import { EnquiryTableComponent } from './enquiry-table/enquiry-table.component';

const routes: Routes = [
  {path:'enquiry-form',component:EnquiryFormComponent},
  {path:'enquiry-table',component:EnquiryTableComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnquiryRoutingModule { }
