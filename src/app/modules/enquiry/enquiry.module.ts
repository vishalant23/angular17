import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EnquiryRoutingModule } from './enquiry-routing.module';
import { EnquiryFormComponent } from './enquiry-form/enquiry-form.component';
import { EnquiryTableComponent } from './enquiry-table/enquiry-table.component';
@NgModule({
  declarations: [EnquiryFormComponent,EnquiryTableComponent],
  imports: [
    CommonModule,
    EnquiryRoutingModule
  ]
})
export class EnquiryModule { }
