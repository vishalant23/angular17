import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StockRoutingModule } from './stock-routing.module';
import { StockTableComponent } from './stock-table/stock-table.component';

@NgModule({
  declarations: [StockTableComponent],
  imports: [
    CommonModule,
    StockRoutingModule
  ]
})
export class StockModule { }
