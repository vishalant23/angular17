import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogActivityRoutingModule } from './log-activity-routing.module';
import { LogActivityTableComponent } from './log-activity-table/log-activity-table.component';

@NgModule({
  declarations: [LogActivityTableComponent],
  imports: [
    CommonModule,
    LogActivityRoutingModule
  ]
})
export class LogActivityModule { }
