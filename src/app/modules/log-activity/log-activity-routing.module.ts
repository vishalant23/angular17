import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogActivityTableComponent } from './log-activity-table/log-activity-table.component';

const routes: Routes = [
  {path:'log-activity-table',component:LogActivityTableComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LogActivityRoutingModule { }
