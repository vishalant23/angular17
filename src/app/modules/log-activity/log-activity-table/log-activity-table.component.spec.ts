import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogActivityTableComponent } from './log-activity-table.component';

describe('LogActivityTableComponent', () => {
  let component: LogActivityTableComponent;
  let fixture: ComponentFixture<LogActivityTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LogActivityTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(LogActivityTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
