import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesOrderRoutingModule } from './sales-order-routing.module';
import { SalesOrderFormComponent } from './sales-order-form/sales-order-form.component';
import { SalesOrderTableComponent } from './sales-order-table/sales-order-table.component';
@NgModule({
  declarations: [SalesOrderFormComponent,SalesOrderTableComponent],
  imports: [
    CommonModule,
    SalesOrderRoutingModule
  ]
})
export class SalesOrderModule { }
