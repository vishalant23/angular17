import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalesOrderFormComponent } from './sales-order-form/sales-order-form.component';
import { SalesOrderTableComponent } from './sales-order-table/sales-order-table.component';
const routes: Routes = [
  {path:'sales-order-form',component:SalesOrderFormComponent},
  {path:'sales-order-table',component:SalesOrderTableComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesOrderRoutingModule { }
