import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyProfileFormComponent } from './company-profile-form/company-profile-form.component';
import { CompanyProfileTableComponent } from './company-profile-table/company-profile-table.component';

const routes: Routes = [
  {path:'company-profile-form',component:CompanyProfileFormComponent},
  {path:'company-profile-table',component:CompanyProfileTableComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyProfileRoutingModule { }
