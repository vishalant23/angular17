import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyProfileRoutingModule } from './company-profile-routing.module';
import { CompanyProfileFormComponent } from './company-profile-form/company-profile-form.component';
import { CompanyProfileTableComponent } from './company-profile-table/company-profile-table.component';
@NgModule({
  declarations: [CompanyProfileFormComponent,CompanyProfileTableComponent],
  imports: [
    CommonModule,
    CompanyProfileRoutingModule
  ]
})
export class CompanyProfileModule { }
