import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyProfileTableComponent } from './company-profile-table.component';

describe('CompanyProfileTableComponent', () => {
  let component: CompanyProfileTableComponent;
  let fixture: ComponentFixture<CompanyProfileTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CompanyProfileTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CompanyProfileTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
