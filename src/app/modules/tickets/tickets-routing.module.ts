import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TicketsFormComponent } from './tickets-form/tickets-form.component';
import { TicketsTableComponent } from './tickets-table/tickets-table.component';
const routes: Routes = [
  {path:'tickets-form',component:TicketsFormComponent},
  {path:'tickets-table',component:TicketsTableComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TicketsRoutingModule { }
