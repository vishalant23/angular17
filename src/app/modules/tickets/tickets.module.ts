import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TicketsRoutingModule } from './tickets-routing.module';
import { TicketsFormComponent } from './tickets-form/tickets-form.component';
import { TicketsTableComponent } from './tickets-table/tickets-table.component';
@NgModule({
  declarations: [TicketsFormComponent,TicketsTableComponent],
  imports: [
    CommonModule,
    TicketsRoutingModule
  ]
})
export class TicketsModule { }
