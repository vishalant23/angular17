import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingSlipTableComponent } from './packing-slip-table.component';

describe('PackingSlipTableComponent', () => {
  let component: PackingSlipTableComponent;
  let fixture: ComponentFixture<PackingSlipTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PackingSlipTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PackingSlipTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
