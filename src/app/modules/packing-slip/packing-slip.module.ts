import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PackingSlipRoutingModule } from './packing-slip-routing.module';
import { PackingSlipFormComponent } from './packing-slip-form/packing-slip-form.component';
import { PackingSlipTableComponent } from './packing-slip-table/packing-slip-table.component';
@NgModule({
  declarations: [PackingSlipFormComponent,PackingSlipTableComponent],
  imports: [
    CommonModule,
    PackingSlipRoutingModule
  ]
})
export class PackingSlipModule { }
