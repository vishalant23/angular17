import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PackingSlipFormComponent } from './packing-slip-form/packing-slip-form.component';
import { PackingSlipTableComponent } from './packing-slip-table/packing-slip-table.component';
const routes: Routes = [
  {path:'packing-slip-form',component:PackingSlipFormComponent},
  {path:'packing-slip-table',component:PackingSlipTableComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PackingSlipRoutingModule { }
