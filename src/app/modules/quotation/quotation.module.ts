import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuotationRoutingModule } from './quotation-routing.module';
import { QuotationFormComponent } from './quotation-form/quotation-form.component';
import { QuotationTableComponent } from './quotation-table/quotation-table.component';
@NgModule({
  declarations: [QuotationFormComponent,QuotationTableComponent],
  imports: [
    CommonModule,
    QuotationRoutingModule
  ]
})
export class QuotationModule { }
