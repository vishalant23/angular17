import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuotationFormComponent } from './quotation-form/quotation-form.component';
import { QuotationTableComponent } from './quotation-table/quotation-table.component';
const routes: Routes = [
  {path:'quotation-form',component:QuotationFormComponent},
  {path:'quotation-table',component:QuotationTableComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuotationRoutingModule { }
