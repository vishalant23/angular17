import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChangePasswordRoutingModule } from './change-password-routing.module';
import { ChangePasswordFormComponent } from './change-password-form/change-password-form.component';

@NgModule({
  declarations: [ChangePasswordFormComponent],
  imports: [
    CommonModule,
    ChangePasswordRoutingModule
  ]
})
export class ChangePasswordModule { }
