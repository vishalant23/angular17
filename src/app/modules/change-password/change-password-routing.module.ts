import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChangePasswordFormComponent } from './change-password-form/change-password-form.component';
const routes: Routes = [
  {path:'change-password-form',component:ChangePasswordFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChangePasswordRoutingModule { }
