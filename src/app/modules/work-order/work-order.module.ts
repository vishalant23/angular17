import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkOrderRoutingModule } from './work-order-routing.module';
import { WorkOrderTableComponent } from './work-order-table/work-order-table.component';

@NgModule({
  declarations: [WorkOrderTableComponent],
  imports: [
    CommonModule,
    WorkOrderRoutingModule
  ]
})
export class WorkOrderModule { }
