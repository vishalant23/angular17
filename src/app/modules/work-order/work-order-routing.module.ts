import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkOrderTableComponent } from './work-order-table/work-order-table.component';

const routes: Routes = [
  {path:'work-order-table',component:WorkOrderTableComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkOrderRoutingModule { }
