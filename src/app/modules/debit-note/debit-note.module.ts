import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DebitNoteRoutingModule } from './debit-note-routing.module';
import { DebitNoteFormComponent } from './debit-note-form/debit-note-form.component';
import { DebitNoteTableComponent } from './debit-note-table/debit-note-table.component';
@NgModule({
  declarations: [DebitNoteFormComponent,DebitNoteTableComponent],
  imports: [
    CommonModule,
    DebitNoteRoutingModule
  ]
})
export class DebitNoteModule { }
