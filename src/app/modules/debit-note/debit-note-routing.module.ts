import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DebitNoteFormComponent } from './debit-note-form/debit-note-form.component';
import { DebitNoteTableComponent } from './debit-note-table/debit-note-table.component';

const routes: Routes = [
  {path:'debit-note-form',component:DebitNoteFormComponent},
  {path:'debit-note-table',component:DebitNoteTableComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DebitNoteRoutingModule { }
