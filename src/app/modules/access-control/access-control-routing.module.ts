import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessControlFormComponent } from './access-control-form/access-control-form.component';
import { AccessControlTableComponent } from './access-control-table/access-control-table.component';
const routes: Routes = [
  
  {path:'access-control-form',component:AccessControlFormComponent},
  {path:'access-control-table',component:AccessControlTableComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessControlRoutingModule { }
