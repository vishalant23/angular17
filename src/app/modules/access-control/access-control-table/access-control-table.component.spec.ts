import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessControlTableComponent } from './access-control-table.component';

describe('AccessControlTableComponent', () => {
  let component: AccessControlTableComponent;
  let fixture: ComponentFixture<AccessControlTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AccessControlTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AccessControlTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
