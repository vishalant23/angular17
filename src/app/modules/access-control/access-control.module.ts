import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessControlRoutingModule } from './access-control-routing.module';
import { AccessControlFormComponent } from './access-control-form/access-control-form.component';
import { AccessControlTableComponent } from './access-control-table/access-control-table.component';


@NgModule({
  declarations: [AccessControlFormComponent,AccessControlTableComponent],
  imports: [
    CommonModule,
    AccessControlRoutingModule
  ]
})
export class AccessControlModule { }
