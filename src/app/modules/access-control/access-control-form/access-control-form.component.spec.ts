import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessControlFormComponent } from './access-control-form.component';

describe('AccessControlFormComponent', () => {
  let component: AccessControlFormComponent;
  let fixture: ComponentFixture<AccessControlFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AccessControlFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AccessControlFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
