import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreditNoteFormComponent } from './credit-note-form/credit-note-form.component';
import { CreditNoteTableComponent } from './credit-note-table/credit-note-table.component';
const routes: Routes = [
  {path:'credit-note-form',component:CreditNoteFormComponent},
  {path:'credit-note-table',component:CreditNoteTableComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditNoteRoutingModule { }
