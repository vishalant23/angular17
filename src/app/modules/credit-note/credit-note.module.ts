import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreditNoteRoutingModule } from './credit-note-routing.module';
import { CreditNoteFormComponent } from './credit-note-form/credit-note-form.component';
import { CreditNoteTableComponent } from './credit-note-table/credit-note-table.component';
@NgModule({
  declarations: [CreditNoteFormComponent,CreditNoteTableComponent],
  imports: [
    CommonModule,
    CreditNoteRoutingModule
  ]
})
export class CreditNoteModule { }
