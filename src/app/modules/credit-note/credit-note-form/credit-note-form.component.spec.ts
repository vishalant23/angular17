import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditNoteFormComponent } from './credit-note-form.component';

describe('CreditNoteFormComponent', () => {
  let component: CreditNoteFormComponent;
  let fixture: ComponentFixture<CreditNoteFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditNoteFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CreditNoteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
