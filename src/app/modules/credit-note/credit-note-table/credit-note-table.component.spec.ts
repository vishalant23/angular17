import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditNoteTableComponent } from './credit-note-table.component';

describe('CreditNoteTableComponent', () => {
  let component: CreditNoteTableComponent;
  let fixture: ComponentFixture<CreditNoteTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditNoteTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CreditNoteTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
