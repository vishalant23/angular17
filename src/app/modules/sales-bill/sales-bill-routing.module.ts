import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalesBillFormComponent } from './sales-bill-form/sales-bill-form.component';
import { SalesBillTableComponent } from './sales-bill-table/sales-bill-table.component';
const routes: Routes = [
  {path:'sales-bill-form',component:SalesBillFormComponent},
  {path:'sales-bill-table',component:SalesBillTableComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesBillRoutingModule { }
