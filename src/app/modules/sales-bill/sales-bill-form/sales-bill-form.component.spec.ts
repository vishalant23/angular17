import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesBillFormComponent } from './sales-bill-form.component';

describe('SalesBillFormComponent', () => {
  let component: SalesBillFormComponent;
  let fixture: ComponentFixture<SalesBillFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SalesBillFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SalesBillFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
