import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesBillTableComponent } from './sales-bill-table.component';

describe('SalesBillTableComponent', () => {
  let component: SalesBillTableComponent;
  let fixture: ComponentFixture<SalesBillTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SalesBillTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SalesBillTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
