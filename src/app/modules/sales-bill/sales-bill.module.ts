import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesBillRoutingModule } from './sales-bill-routing.module';
import { SalesBillTableComponent } from './sales-bill-table/sales-bill-table.component';
import { SalesBillFormComponent } from './sales-bill-form/sales-bill-form.component';
@NgModule({
  declarations: [SalesBillTableComponent,SalesBillFormComponent],
  imports: [
    CommonModule,
    SalesBillRoutingModule
  ]
})
export class SalesBillModule { }
