import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProformaInvoiceTableComponent } from './proforma-invoice-table.component';

describe('ProformaInvoiceTableComponent', () => {
  let component: ProformaInvoiceTableComponent;
  let fixture: ComponentFixture<ProformaInvoiceTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProformaInvoiceTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ProformaInvoiceTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
