import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProformaInvoiceFormComponent } from './proforma-invoice-form/proforma-invoice-form.component';
import { ProformaInvoiceTableComponent } from './proforma-invoice-table/proforma-invoice-table.component';
const routes: Routes = [
  {path:'proforma-invoice-form',component:ProformaInvoiceFormComponent},
  {path:'proforma-invoice-table',component:ProformaInvoiceTableComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProformaInvoiceRoutingModule { }
