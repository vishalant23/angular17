import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProformaInvoiceFormComponent } from './proforma-invoice-form.component';

describe('ProformaInvoiceFormComponent', () => {
  let component: ProformaInvoiceFormComponent;
  let fixture: ComponentFixture<ProformaInvoiceFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProformaInvoiceFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ProformaInvoiceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
