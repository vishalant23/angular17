import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProformaInvoiceRoutingModule } from './proforma-invoice-routing.module';
import { ProformaInvoiceFormComponent } from './proforma-invoice-form/proforma-invoice-form.component';
import { ProformaInvoiceTableComponent } from './proforma-invoice-table/proforma-invoice-table.component';
@NgModule({
  declarations: [ProformaInvoiceFormComponent,ProformaInvoiceTableComponent],
  imports: [
    CommonModule,
    ProformaInvoiceRoutingModule
  ]
})
export class ProformaInvoiceModule { }
