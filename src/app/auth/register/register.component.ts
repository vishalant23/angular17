import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators, FormGroupDirective, ValidationErrors} from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.scss'
})
export class RegisterComponent implements OnInit {
  registerGroup!: FormGroup;
  termsConditions:boolean=false;

  constructor(private formBuilder: FormBuilder,private toaster: ToastrService) {

  }
  get form() { return this.registerGroup.controls };

  ngOnInit(): void {
    this.registerForm();
  }

  registerForm(){
    this.registerGroup = this.formBuilder.group({

      rname: ['', Validators.required],
      remail: [, [Validators.required,Validators.email]],
      rpassword: ['', [Validators.required, Validators.pattern(/^[A-Z][A-Za-z0-9]*[!@#$%^&*()_+[\]{};':"<>?/\\|,.-]+[0-9]+[A-Za-z0-9!@#$%^&*()_+[\]{};':"<>?/\\|,.-]*$/)]],
      rconfirmpassword: ['', [Validators.required, Validators.pattern(/^[A-Z][A-Za-z0-9]*[!@#$%^&*()_+[\]{};':"<>?/\\|,.-]+[0-9]+[A-Za-z0-9!@#$%^&*()_+[\]{};':"<>?/\\|,.-]*$/)]],
     
    })
  }

  termCondition(event:any){
    if(event.target.checked){
      this.termsConditions=true;
    }else{
      this.termsConditions=false;
    }
  }

  registerNow(){
    console.log("get values",this.registerGroup);
    if(this.registerGroup.valid && this.termsConditions){
      this.toaster.success("User Register Successfully")
      this.registerGroup.reset()
    }else{
      this.toaster.warning("Please fill form correctly")
      this.registerGroup.markAllAsTouched()
    }
  }

}
