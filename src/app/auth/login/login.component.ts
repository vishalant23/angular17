import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators, FormGroupDirective, ValidationErrors} from "@angular/forms";
import { ToastrService } from 'ngx-toastr'
import { CryptojsService } from '../../shared/service/cryptojs.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent implements OnInit{
  loginGroup!: FormGroup;

  constructor(private formBuilder: FormBuilder,private toaster: ToastrService,private cryptojs:CryptojsService){

  }

  ngOnInit(): void {
    this.loginForm();
  }

  get form() { return this.loginGroup.controls };

  loginForm(){
    this.loginGroup = this.formBuilder.group({
      lemail: [, [Validators.required,Validators.email]],
      lpassword: ['', [Validators.required, Validators.pattern(/^[A-Z][A-Za-z0-9]*[!@#$%^&*()_+[\]{};':"<>?/\\|,.-]+[0-9]+[A-Za-z0-9!@#$%^&*()_+[\]{};':"<>?/\\|,.-]*$/)]],
    })
  }

  
  rememberMe(event:any){
    if(this.loginGroup.valid){
      if(event.target.checked){
        localStorage.setItem("lemail",this.loginGroup.controls['lemail'].value)
        this.cryptojs.encryptLocalLPassStorage(this.loginGroup.controls['lpassword'].value)
        this.toaster.success("Successfully save login credentials")
      }
    }else{
      this.toaster.warning("Email and Password are required")
      event.target.checked=false;
    }
  }

  getRememberData(){
    const LPass = this.cryptojs.decryptLocalLPassStorage()
  }

  loginNow(){
    console.log("get values",this.loginGroup);
    if(this.loginGroup.valid){
      this.toaster.success("User Login Successfully")
      this.loginGroup.reset()
    }else{
      this.toaster.warning("Please fill form correctly")
      this.loginGroup.markAllAsTouched()
    }
  }

}
