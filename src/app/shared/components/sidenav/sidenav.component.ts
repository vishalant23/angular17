import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrl: './sidenav.component.scss'
})
export class SidenavComponent implements OnInit{

  constructor(private router:Router){

  }

  ngOnInit(): void {
  
  }

  goToLogin(){
    this.router.navigateByUrl("/login")
  }

  goTo(parent:string,child:string){
    if(child === ''){
      this.router.navigateByUrl(`/${parent}`)
    }else{
      this.router.navigateByUrl(`/${parent}/${child}`)
    }
    
   
  }

  

}
